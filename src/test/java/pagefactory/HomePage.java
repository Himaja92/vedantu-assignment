package pagefactory;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import java.util.concurrent.TimeUnit;

public class HomePage {

    public HomePage(WebDriver driver) {
        PageFactory.initElements(driver, this);
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
    }

    @FindBy(xpath = "//input[@placeholder='choose a room name']")
    private WebElement createRoomTxtBx;

    @FindBy(xpath = "//button[text()='Start a chat']")
    private WebElement startChatBtn;

    public void enterRoomName(String roomName) {
        createRoomTxtBx.sendKeys(roomName);
    }

    public void clickStartChat() {
        startChatBtn.click();
    }


}
