package pagefactory;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.concurrent.TimeUnit;

public class JoinVideoPage {

    WebDriverWait wait;

    public JoinVideoPage(WebDriver driver) {
        PageFactory.initElements(driver, this);
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        wait = new WebDriverWait(driver, 20);
    }

    @FindBy(xpath = "//button/span[text()='Allow camera access']")
    private WebElement cameraAccessBtn;

    @FindBy(xpath = "//button/span[text()='Allow microphone access']")
    private WebElement microPhoneAccessBtn;

    @FindBy(xpath = "//button[text()='Join Call']")
    private WebElement joinCallBtn;

    @FindBy(xpath = "//span[text()='Invite']")
    private WebElement inviteBtn;

    public void clickCameraAccess() {
        cameraAccessBtn.click();
    }

    public void clickMicAccess() {
        microPhoneAccessBtn.click();
    }

    public void clickJoinCallBtn() {
        joinCallBtn.click();
        wait.until(ExpectedConditions.visibilityOf(inviteBtn));
    }
}
