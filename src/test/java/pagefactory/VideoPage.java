package pagefactory;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.concurrent.TimeUnit;

public class VideoPage {

    private WebDriver driver;
    private WebDriverWait wait;

    public VideoPage(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        wait = new WebDriverWait(driver, 30);
    }

    @FindBy(xpath = "//button/span[text()='Invite']")
    private WebElement inviteBtn;

    @FindBy(xpath = "//input[@placeholder='Your name (click to edit)']")
    private WebElement userName;


    public void setUserName(String name) {
        wait.until(ExpectedConditions.visibilityOf(userName));
        userName.sendKeys(Keys.ENTER);
        userName.sendKeys(name);
        userName.sendKeys(Keys.ENTER);
        wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.xpath("//input[@value='" + name + "']"))));
    }

    public boolean verifyParticipant(String userName) {
        return driver.findElement(By.xpath("//li[text()='" + userName + "']")).isDisplayed();
    }
}
