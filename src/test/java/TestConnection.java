import io.github.bonigarcia.wdm.WebDriverManager;
import lib.CommonFlows;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

public class TestConnection {

    String roomName;
    String hostUser;
    String guestUser;
    WebDriver hostDriver;
    WebDriver guestDriver;

    @BeforeTest
    public void setup() {
        roomName = "TestingRoom";
        hostUser = "host";
        guestUser = "guest";
        WebDriverManager.chromedriver().setup();
    }

    @Test(description = "Verify connection for meeting room")
    public void connectToChatRoom() {
        DriverManager driverManager = new DriverManager();
        CommonFlows commonFlows = new CommonFlows();

        //Get driver for client1
        hostDriver = driverManager.getDriver();
        //Join the meeting room for client1 and set name
        commonFlows.joinMeetingRoom(hostDriver, roomName, hostUser);

        //Get driver for client2
        guestDriver = driverManager.getDriver();
        //Join meeting room for client2 and set name
        commonFlows.joinMeetingRoom(guestDriver, roomName, guestUser);

        //Verify whether connection established by checking participants on both clients
        commonFlows.verifyParticipant(hostDriver, guestUser);
        commonFlows.verifyParticipant(guestDriver, hostUser);
    }

    @AfterTest
    public void tearDown() {
        hostDriver.quit();
        guestDriver.quit();
    }
}
