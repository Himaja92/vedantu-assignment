import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;

import java.util.Arrays;

public class DriverManager {

    public WebDriver getDriver() {
        ChromeOptions options = new ChromeOptions();
        options.addArguments(Arrays.asList("disable-infobars",
                "ignore-certificate-errors", "start-maximized", "use-fake-ui-for-media-stream"));
        return new ChromeDriver(options);
    }
}
