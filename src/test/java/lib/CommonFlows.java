package lib;

import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import pagefactory.HomePage;
import pagefactory.JoinVideoPage;
import pagefactory.VideoPage;

public class CommonFlows {


    //Join meeting room for a client and set userName
    public void joinMeetingRoom(WebDriver driver, String roomName, String userName) {
        driver.get("https://talky.io");
        HomePage homePage = new HomePage(driver);
        homePage.enterRoomName(roomName);
        homePage.clickStartChat();

        JoinVideoPage joinVideoPage = new JoinVideoPage(driver);
        joinVideoPage.clickJoinCallBtn();
        Assert.assertEquals(driver.getCurrentUrl(), "https://talky.io/" + roomName);

        VideoPage videoPage = new VideoPage(driver);
        videoPage.setUserName(userName);
    }

    //Verify the participant given is in meeting client meeting room.
    public void verifyParticipant(WebDriver driver, String participant) {
        VideoPage videoPage = new VideoPage(driver);
        Assert.assertTrue(videoPage.verifyParticipant(participant));
    }
}

